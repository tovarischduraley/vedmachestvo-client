import { AuthController } from '@/controllers/AuthController'

export default async (to, from, next) => {
  if (to.path !== '/login' && to.path !== '/signup') {
    try {
      await AuthController.auth()
    } catch (err) {
      next({ path: '/login' })
    }
  }
  next()
}
