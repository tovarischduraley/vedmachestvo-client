import { createRouter, createWebHistory } from 'vue-router'
import { routes } from '@/router/routes'
import redirect from '@/router/middleware/redirect'

const router = createRouter({
  history: createWebHistory(),
  routes,
})

router.beforeEach(redirect)
export default router
