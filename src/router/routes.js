import HomePage from '@/pages/home/HomePage.vue'
import LawsList from '@/pages/laws/laws-list/LawsList.vue'
import TestComponent from '@/pages/test/TestComponent.vue'
import LoginPage from '@/pages/login/LoginPage.vue'
import UsersList from '@/pages/users/users-list/UsersList.vue'
import UserCreate from '@/pages/users/user-create/UserCreate.vue'
import QuestsList from '@/pages/quests/quest-list/QuestsList.vue'
import LawDetail from '@/pages/laws/law-detali/LawDetail.vue'
import QuestDetail from '@/pages/quests/quest-detali/QuestDetail.vue'
import UserDetail from '@/pages/users/user-detali/UserDetail.vue'
import SignupPage from '@/pages/signup/SignupPage.vue'

export const routes = [
  { path: '/home', component: HomePage, alias: '/' },
  { path: '/laws', component: LawsList },
  { path: '/test', component: TestComponent },
  { path: '/login', component: LoginPage },
  { path: '/signup', component: SignupPage },
  { path: '/users', component: UsersList },
  { path: '/witchers', component: UsersList },
  { path: '/users/create', component: UserCreate },
  { path: '/quests', component: QuestsList, name: 'QuestsListPage' },
  { path: '/laws/:id', component: LawDetail, props: true },
  { path: '/quests/:id', component: QuestDetail, props: true },
  { path: '/users/:id', component: UserDetail, props: true, name: 'UserDetailPage' },
]
