export const segmentedOptions = {
  witcher: {
    users: [
      { value: 'mine', title: 'Мои' },
      { value: 'dead', title: 'Мертвые' },
      { value: 'banned', title: 'Исключенные' },
    ],
  },
  headmaster: {
    witchers: [
      { value: 'all', title: 'Все' },
      { value: 'banned', title: 'Исключенные' },
    ],
    users: [
      { value: 'all', title: 'Все' },
      { value: 'dead', title: 'Мертвые' },
      { value: 'withoutWitcher', title: 'Без ведьмака' },
      { value: 'banned', title: 'Исключенные' },
    ],
  },
}

export const segmentedValue = {
  witcher: {
    users: 'mine',
  },
  headmaster: {
    users: 'all',
    witchers: 'all',
  },
}
