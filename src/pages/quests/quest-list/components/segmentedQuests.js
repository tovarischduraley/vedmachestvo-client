export const segmentedOptions = {
  witcher: [
    { value: 'mine', title: 'Мои' },
    { value: 'in-review', title: 'На проверку' },
    { value: 'done', title: 'Готовые' },
    { value: 'no-student', title: 'Без ученика' },
  ],
}

export const segmentedValue = {
  witcher: 'mine',
}
