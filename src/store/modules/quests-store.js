export const questsStore = {
  namespaced: true,
  state: () => ({
    quests: [],
    quest: {},
    statuses: {
      New: {
        name: 'Новое',
        color: 'blue',
      },
      ToDo: {
        name: 'To do',
        color: 'orange',
      },
      InProgress: {
        name: 'Выполняется',
        color: 'green',
      },
      InReview: {
        name: 'На проверке',
        color: 'yellow',
      },
      Done: {
        name: 'Выполнено',
        color: '#AED6F1',
      },
      Backlog: {
        name: 'Бэклог',
        color: 'gray',
      },
    },
  }),
  mutations: {
    SET_QUESTS(state, data) {
      state.quests = data
    },
    SET_QUEST(state, data) {
      state.quest = data
    },
  },
}
