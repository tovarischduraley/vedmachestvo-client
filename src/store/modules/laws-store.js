export const lawsStore = {
  namespaced: true,
  state: () => ({
    laws: [],
    law: {},
    studentModal: {
      value: false,
      mode: '',
    },
    formCreateStudent: {
      name: '',
      description: '',
      location: '',
      agility: 1,
      strength: 1,
      intelligence: 1,
      email: '',
      password: '',
      role: 'student',
      law_id: null,
      witcher_id: null,
    },
  }),
  mutations: {
    SET_LAWS(state, data) {
      state.laws = data
    },
    SET_LAW(state, data) {
      state.law = data
    },
    UPDATE_FORM_CREATE_STUDENT(state, data) {
      state.formCreateStudent = {
        ...state.formCreateStudent,
        ...data,
      }
    },
    TOGGLE_STUDENT_MODAL(state, data) {
      state.studentModal = data
    },
  },
  actions: {
    updateFormCreateStudent({ commit, state }, data) {
      commit('UPDATE_FORM_CREATE_STUDENT', data)
    },
    toggleStudentModal({ commit, state }, value) {
      commit('TOGGLE_STUDENT_MODAL', value)
    },
    resetCreateStudentForm({ commit }) {
      commit('UPDATE_FORM_CREATE_STUDENT', {
        name: '',
        description: '',
        location: '',
        agility: 1,
        intelligence: 1,
        strength: 1,
        email: '',
        password: '',
        witcher_id: null,
      })
    },
  },
}
