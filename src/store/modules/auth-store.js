export const authStore = {
  namespaced: true,
  state: () => ({
    user: {},
  }),
  mutations: {
    SET_USER(state, data) {
      state.user = data
    },
  },
  getters: {
    isWitcher: (state) => state.user.role === 'witcher',
    isStudent: (state) => state.user.role === 'student',
    isDeath: (state) => state.user.role === 'death',
    isHeadmaster: (state) => state.user.role === 'headmaster',
  },
}
