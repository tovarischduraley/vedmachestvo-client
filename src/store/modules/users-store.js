export const usersStore = {
  namespaced: true,
  state: () => ({
    users: [],
    currentUser: {},
  }),
  mutations: {
    SET_USERS(state, data) {
      state.users = data
    },
    SET_CURRENT_USER(state, data) {
      state.currentUser = data
    },
  },
}
