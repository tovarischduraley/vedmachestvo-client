import { createStore } from 'vuex'
import { authStore } from '@/store/modules/auth-store'
import { usersStore } from '@/store/modules/users-store'
import { questsStore } from '@/store/modules/quests-store'
import { lawsStore } from '@/store/modules/laws-store'

const store = createStore({
  modules: {
    authStore,
    usersStore,
    questsStore,
    lawsStore,
  },
})

export default store
