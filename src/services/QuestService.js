import { HttpClient } from '@/api/HttpClient'

export class QuestService {
  static loadQuestDetail(questId) {
    return HttpClient.get('/quests/' + questId)
  }

  static loadQuestList(params) {
    return HttpClient.get('/quests', {
      params,
    })
  }

  static createQuest(data) {
    return HttpClient.post('/quests', data)
  }
  static updateQuest(id, data) {
    return HttpClient.patch('/quests/' + id, data)
  }

  static removeQuest(id) {
    return HttpClient.delete('/quests/' + id)
  }

  static checkQuest(id) {
    return HttpClient.get('/quests/' + id + '/check')
  }
}
