import { HttpClient } from '@/api/HttpClient'

export class UserService {
  static loadUsers(params) {
    return HttpClient.get('/users', { params })
  }

  static getUser(userId) {
    return HttpClient.get('/users/' + userId)
  }

  static createUser(data) {
    return HttpClient.post('/users', data)
  }

  static editUser(userId, data) {
    return HttpClient.patch('/users/' + userId, data)
  }

  static killUser(userId) {
    return HttpClient.post('/users/' + userId + '/kill')
  }

  static excludeStudent(userId) {
    return HttpClient.post('/users/' + userId + '/ban')
  }
  static includeStudent(userId) {
    return HttpClient.post('/users/' + userId + '/unban')
  }

  static convertToWitcher(userId) {
    return HttpClient.post('/users/' + userId + '/upgrade')
  }
}
