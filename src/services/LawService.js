import { HttpClient } from '@/api/HttpClient'

export class LawService {
  static loadLawsList(params) {
    return HttpClient.get('/laws', { params })
  }

  static loadLawsDetail(lawId) {
    return HttpClient.get('/laws/' + lawId)
  }
  static createLaw(data) {
    return HttpClient.post('/laws', data)
  }

  static createStudent(lawId, data) {
    return HttpClient.post('/laws/' + lawId + '/upgrade', data)
  }

  static removeLaw(lawId) {
    return HttpClient.delete('/laws/' + lawId)
  }
}
