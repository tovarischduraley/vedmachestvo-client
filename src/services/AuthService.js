import { HttpClient } from '@/api/HttpClient'

export class AuthService {
  static getUser() {
    return HttpClient.get('/me')
  }

  static logout() {
    return HttpClient.post('/logout')
  }

  static signup(data) {
    return HttpClient.post('/users', data)
  }

  static signin(data) {
    return HttpClient.post('/login', data)
  }
}
