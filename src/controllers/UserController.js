import { notification } from 'ant-design-vue'
import { UserService } from '@/services/UserService'
import store from '@/store'

export class UserController {
  static async loadUsers(props) {
    try {
      const params = {}
      if (props && props.segmentedValue === 'dead') {
        params.is_dead = true
      } else if (props && props.segmentedValue === 'banned') {
        params.is_banned = true
      }
      if (store.getters['authStore/isWitcher']) {
        params.role = 'student'
        if ((props && props.segmentedValue === 'mine') || (props && props.isCreateForm)) {
          params.witcher_id = store.state.authStore.user.ID
        }
      } else if (store.getters['authStore/isHeadmaster']) {
        if (window.location.pathname === '/users') {
          params.role = 'student'
          if (props && props.segmentedValue === 'withoutWitcher') {
            params.witcher_id = 'null'
          }
        } else if (window.location.pathname === '/witchers') {
          params.role = 'witcher'
        } else if (props.witcher_id) {
          params.role = 'student'
          params.witcher_id = props.witcher_id
        } else if (props.role) {
          params.role = props.role
        }
      }

      const response = await UserService.loadUsers(params)

      await store.commit('usersStore/SET_USERS', response.data)
    } catch (error) {
      notification.error({
        message: 'Error',
        description: 'Failed to load users',
      })
    }
  }

  static async getUser(userId) {
    try {
      const response = await UserService.getUser(userId)
      await store.commit('usersStore/SET_CURRENT_USER', response.data)
    } catch (err) {
      notification.error({
        message: 'Error',
        description: 'Failed to load user',
      })
    }
  }

  static async createUser(data) {
    try {
      return await UserService.createUser(data)
    } catch (err) {
      notification.error({
        message: 'Error',
        description: 'Failed to create user',
      })
      throw err
    }
  }

  static async editUser(data) {
    try {
      const userId = data.id
      data.id = undefined
      return await UserService.editUser(userId, data)
    } catch (err) {
      notification.error({
        message: 'Error',
        description: 'Failed to edit user',
      })
      throw err
    }
  }

  static async killUser(userId) {
    try {
      return await UserService.killUser(userId)
    } catch (err) {
      notification.error({
        message: 'Error',
        description: 'Failed to kill user',
      })
      throw err
    }
  }

  static async excludeStudent(userId) {
    try {
      return await UserService.excludeStudent(userId)
    } catch (err) {
      notification.error({
        message: 'Error',
        description: 'Failed to exclude user',
      })
      throw err
    }
  }
  static async includeStudent(userId) {
    try {
      return await UserService.includeStudent(userId)
    } catch (err) {
      notification.error({
        message: 'Error',
        description: 'Failed to include user',
      })
      throw err
    }
  }

  static async convertToWitcher(userId) {
    try {
      return await UserService.convertToWitcher(userId)
    } catch (err) {
      notification.error({
        message: 'Error',
        description: 'Failed to upgrade student to witcher',
      })
      throw err
    }
  }
}
