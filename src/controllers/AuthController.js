import { AuthService } from '@/services/AuthService'
import store from '@/store'
import { notification } from 'ant-design-vue'

export class AuthController {
  static async auth() {
    const response = await AuthService.getUser()
    await store.commit('authStore/SET_USER', response.data)
  }

  static async signup(data) {
    try {
      await AuthService.signup(data)
    } catch (err) {
      notification.error({
        message: 'Authorization error',
        description: err.response.data.error,
        onClick: () => {
          console.log('Notification Clicked!')
        },
      })
      throw err
    }
  }

  static async signin(data) {
    try {
      await AuthService.signin(data)
    } catch (err) {
      notification.error({
        message: 'Authorization error',
        description: err.response.data.error,
        onClick: () => {
          console.log('Notification Clicked!')
        },
      })
    }
  }
  static async logout() {
    try {
      await AuthService.logout()
    } catch (err) {
      notification.error({
        message: 'Authorization error',
        onClick: () => {
          console.log('Notification Clicked!')
        },
      })
    }
  }
}
