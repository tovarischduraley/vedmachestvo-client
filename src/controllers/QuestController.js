import { notification } from 'ant-design-vue'
import { QuestService } from '@/services/QuestService'
import store from '@/store'

export class QuestController {
  static async loadQuestDetail(questId) {
    try {
      const response = await QuestService.loadQuestDetail(questId)
      return await store.commit('questsStore/SET_QUEST', response.data)
    } catch (err) {
      notification.error({
        message: 'Error',
        description: 'Failed to load quest detail',
      })
      throw err
    }
  }

  static async loadQuestList(props) {
    try {
      const params = {}
      if (store.getters['authStore/isWitcher']) {
        params.witcher_id = store.state.authStore.user.ID
        if (props && props.segmentedValue === 'in-review') {
          params.status = 'InReview'
        } else if (props && props.segmentedValue === 'done') {
          params.status = 'Done'
        } else if (props && props.segmentedValue === 'no-student') {
          params.student_id = 'null'
        }
      } else if (store.getters['authStore/isStudent']) {
        params.student_id = props.student_id
      } else if (store.getters['authStore/isDeath']) {
        params.status = 'InProgress'
      }

      const response = await QuestService.loadQuestList(params)
      await store.commit('questsStore/SET_QUESTS', response.data)
    } catch (err) {
      notification.error({
        message: 'Error',
        description: 'Failed to load quests',
      })
    }
  }

  static async updateQuest(id, params) {
    try {
      if (params.student_id) {
        params.status = 'ToDo'
      }
      await QuestService.updateQuest(id, params)
    } catch {
      notification.error({
        message: 'Error',
        description: 'Failed to update quest',
      })
    }
  }

  static async createQuest(data) {
    try {
      await QuestService.createQuest(data)
    } catch {
      notification.error({
        message: 'Error',
        description: 'Failed to create quest',
      })
    }
  }

  static async removeQuest(id) {
    try {
      await QuestService.removeQuest(id)
    } catch {
      notification.error({
        message: 'Error',
        description: 'Failed to remove quest',
      })
    }
  }

  static async checkQuest(id) {
    try {
      return await QuestService.checkQuest(id)
    } catch {
      notification.error({
        message: 'Error',
        description: 'Failed to check quest',
      })
    }
  }
}
