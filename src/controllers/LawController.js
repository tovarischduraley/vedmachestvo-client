import { notification } from 'ant-design-vue'
import { LawService } from '@/services/LawService'
import store from '@/store'

export class LawController {
  static async loadLawsList() {
    try {
      const params = {}
      if (store.getters['authStore/isWitcher']) {
        params.witcher_id = store.state.authStore.user.ID
      }
      const response = await LawService.loadLawsList(params)
      await store.commit('lawsStore/SET_LAWS', response.data)
    } catch (err) {
      notification.error({
        message: 'Error',
        description: 'Failed to load laws',
      })
      throw err
    }
  }

  static async loadLawsDetail(lawId) {
    try {
      const params = {}
      if (store.getters['authStore/isWitcher']) {
        params.witcher_id = store.state.authStore.user.ID
      }
      const response = await LawService.loadLawsDetail(lawId)
      await store.commit('lawsStore/SET_LAW', response.data)
    } catch (err) {
      notification.error({
        message: 'Error',
        description: 'Failed to load laws',
      })
      throw err
    }
  }

  static createLaw(data) {
    try {
      data.witcher_id = store.state.authStore.user.ID
      return LawService.createLaw(data)
    } catch (err) {
      notification.error({
        message: 'Error',
        description: 'Failed to create law',
      })
    }
  }

  static createStudent(data) {
    try {
      const lawId = data.law_id
      return LawService.createStudent(lawId, { ...data, law_id: undefined })
    } catch (err) {
      notification.error({
        message: 'Error',
        description: 'Failed to create student from law',
        onClick: () => {
          console.log('Notification Clicked!')
        },
      })
      throw err
    }
  }

  static async removeLaw(lawId) {
    try {
      return await LawService.removeLaw(lawId)
    } catch (err) {
      notification.error({
        message: 'Error',
        description: 'Failed to remove law',
      })
    }
  }
}
