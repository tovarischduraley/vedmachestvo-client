export const navigationLinks = {
  users: {
    url: '/users',
    title: 'Пользователи',
  },
  laws: {
    url: '/laws',
    title: 'Права неожиданности',
  },
  quests: {
    url: '/quests',
    title: 'Испытания',
  },
  witchers: {
    url: '/witchers',
    title: 'Ведьмаки',
  },
}
