module.exports = {
  root: true,
  env: {
    node: true,
  },
  extends: ['plugin:vue/vue3-essential', 'eslint:recommended', 'prettier'],
  parserOptions: {
    parser: '@babel/eslint-parser',
  },
  plugins: ['prettier'],
  rules: {
    'prettier/prettier': [
      2,
      {
        singleQuote: true,
        semi: false,
        printWidth: 100,
        'space-before-function-paren': 0,
        endOfLine: 'auto',
      },
    ],
    'no-unused-vars': 'off',
    quotes: [2, 'single', { avoidEscape: true }],
    semi: 0,
    indent: [
      2,
      2,
      {
        SwitchCase: 1,
        ignoredNodes: ['ConditionalExpression'],
      },
    ],
    'space-before-function-paren': 0,
    'max-len': [1, { code: 100 }],
  },
  overrides: [
    {
      files: ['src/views/**/*.vue'],
      rules: {
        'vue/multi-word-component-names': 0,
      },
    },
  ],
  ignorePatterns: ['node_modules', 'build', 'dist', 'public'],
}
